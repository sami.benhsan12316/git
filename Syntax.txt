Full Git Syntax: (Syntax-Role): 

5/ git clone "@ssh" - Tcolonilk copie ml repo li fl remote 

1/ git init --initial-branch=main - Tinisilisi repo git 3al branch main 

13/ git status - Ta3tik l'état actuel du repo 

2/ git add .  - Tajouti tt les fichiers ml working dir ll staging area 

3/ git commit : 
    git commit -m "..." - Tajouti tt les fichiers ml staging area ll local repo
    git commit --amend - Tlaha9 el modif li 3mélto 3la ekhr commit 3ou4 mata3mlou commit wahdo 

4/ git push : 
    - git push - Push fl Cas normal 
    - git push --force  -  Ki thb tforci push  
    - git push --set-upstream origin <esm_branch>  - Ki thb tpushi branch owl mara 
    - git push origin --delete feature - Ki thb tfasakh branch fl remote 
    - git push origin --tags - Ki thb tpushi les tags li sna3thom 
    - git push --delete origin <esm_tag> - Tfasakh tag ml remote 
    

6/ git remote : 
    git remote -v - Ta3tik repo li inti fih mnin ypulli/ypushi 
    git remote add origin @ssh_repo - Torbot repo local bl repo remote 

7/ git branch: 
    - git branch  - Tlistilik les branches dispo w thot etoile 3ali inti fiha 
    - git branch --set-upstream-to=origin/<esm_branch> <esm_branch> - Torbot branch b branch 
    - git branch -D <esm_branch> - Tfasakhlk branch fl local 
    - 

8/ git log - Ta3tik l'ensemble des commits kol 

9/ git checkout : 
    git checkout <commit/tag/branch> - Yhawl bik ll <commit/tag/branch>
    git checkout -b <esm_branch> - Tesna3lk branch w thawlélha 

10/ git stash: 
    git stash: Tsavilk el khdma actuelle 
    git stash pop: Traja3lk el khédma eli savitha fl working dir 

11/ git reset: 
    git reset --hard HEAD~n - Tfasakhlk ekhr n commit ml local (modif fl working dir + commit) 
    git reset --soft HEAD~n -  Tfasakhlk ekhr n commit ml local (commit KHW .. modif fl working dir ya93ad) 

12/ git tag: 
    git tag - Tlistilik les tag li moujoudin 
    git tag <esm_tag> - Tasna3lk Lightweight tag 
    git tag -a <esm_tag> -m "message" - Tesna3lk Annotated tag 
    git tag -d <esm_tag> - Tfasakh tag ml local 


14/ git merge: 
    git merge B - Tmergi B fl branch li bch tlansi fiha hal commande  
    git merge B --strategy=recursive - Tmergi B fl branch li bch tlansi fiha hal commande en fixant la strategy de merge 
                                       a recursive 
15/ git rebase: 
    git rebase B - Trebasi B fl branch li bch tlansi fiha hal commande 
    git rebase --continue - Bch tkaml rebase ba3d matkoun sala7t el conflict 

16/ git rev-parse:  
    git rev-parse--is-inside-work-tree # Tkharajlk true kéno repo git / false ou un msg d'erreur si nn