PB1: Lansit commande git fi wost dossier fl local " fatal : not a git repository......    . git " 
SOL: Ma3neha mazel mawalech repo git (ltw la wala fih dossier  .git) lezm te9lbo l local 
git repository kifeh ? 
	-> git init --initial-branch=main (voir capture git init ) : 
	Ta3ml git init yjik message " Initialized empty git repository in.... "  
    -> Verif: 
        1/ ls -a | grep .git # ken tal9a .git wost repo -> git repo .. si nn mch git repo 
		2/ git rev-parse --is-inside-work-tree # Tkharajlk true kéno repo git / false ou un msg d'erreur si nn
				
PB2: git push jébtli haka "Fatal :  No configured push destination" 
SOL:  destination li t7éb ta3ml fiha l push mté3k ltw local repo maya3rafhéch  
	-> Repo local mte3na mazel mch marbout bl repo remote... Maya3rf chy 3lih
    Solution : Lézm torbot bin repo remote w local !!!
        -> Comment ? : 
            git remote add origin @SSH/HTTP # tal9aha fl documentation ( Voir capture git remote add esm_repo @ ) 
                git remote : commande nesta3mlouha bch nmanagiw les repos remote associés m3a repo local mte3na 
                origin : Tt simplement tarbija (label) ll repo remote 
                @SSH/HTTP : nafs li 7kinéh .. préfèrable @SSH							
                -> Haka Repo Local wala marbout bl Repo Remote # Local wala ya3rf li ..

PB3: Repouét marboutin mriglin .. git push -> " The current branch master has no upstream branch " 
SOL: ma3néha branch main ta3 local mazélt mata3rfch  branch li lézm tpushi fiha fl remote 
    Solution ? 
        Norbot branch main ta3 local bl branch main ta3 remote 
        git push --set-upstream origin main  ( tjik déja en suggestion ba3d mata3ml git push 7aff ) 
        ( nzido git push -- héthom : blou8a okhra faxili upstream ta3 master branch  (plus généralement branche li 
        lansit fiha hal commande #Thbét béhy Q7 ) mté3i howa master ta3 origin ... ma3néha branch master ta3 local bch tpushi fl 
        branch master ta3 remote ) tmchi t7éll repo git mté3k tal9ah mriguél taw 

PB4: error: Your local changes to the following files would be overwritten by checkout
SOL: Generally ay checkout (branch/commit/tag) tq (branch/commit/tag) LI HAWELTLOU NE CONTIENT PAS LA MM 
VERSION DU FICHIER MODIFIEE 
    -> git stash w ba3d a3ml checkout mte3k 

PB5: error: cannot rebase: You have unstaged changes.
	 error: Please commit or stash them.
SOL:  Commiti kol chy 9bal matrebasi branche fi branche (git status lézmou nthif) si nn kén yébda 3andk modif 
yel3ab w thb trebasi

